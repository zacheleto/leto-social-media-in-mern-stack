# Leto Social Media in MERN Stack

A cool social media project that was built from scratch using the MVC pattern in Node.js to create it's API endpoints and React in the Frontend

### Features

* you can sign up, 
* sign in, 
* sign in with Google and any other social media can be implemented easily, 
* you can read recent posts, 
* can read  single posts  
* can see users list but can't view their profile pages unless you are sign in
* can't comment on single posts unless you are sign in
* after register you'll be redirected to login

* after login you can do the following:
* you'll be having a role of subscriber
* you'll be able to view other users profiles
* you'll be able to follow them
* you'll be able to unfollow them
* you'll be able to look for users in find people tab in the navbar
* you'll be able to like && unlike posts
* you'll be able to comment posts and remove comments
* you'll be able to create your own posts
* you'll be able to edit your profile details 
* you'll get a default avatar profile which you can change after editing your profile
* posts with no photo will be getting a default avatar photo
* you'll be able to remove your profile
* all posts were created by you will be removed using the Pre middleware in the users Model
* users can't edit or delete others profiles nor posts
* you can't access protected routes from urls

* Admin can edit and remove users profiles and posts



### Prerequisites

* Node.JS installed
* mongoDB cluster or old mlab account since it joined mongoDB family
* your browser

### Installing

* git clone https://github.com/letowebdev/Leto_SM_In_NodeJS_and_React.git
* npm install
* create an .env file in the api folder which contains the following:
* MONGO_URI=connect to your cluster here
* PORT=8000

CLIENT_URL=http://localhost:3000

JWT_SECRET=write any random secret



## Website
* Check the api endpoints
* https://leto-sm-api.herokuapp.com/api

* React frontEnd that fetches from the API
* https://leto-sm-react-frontend.herokuapp.com/

## Built With

* [Node.JS, express]
* [mongoDB, mongoose]
* [npm packages that you can read in the package.JSON file]
* [React.JS]


## Author

* **Zache Abdelatif (Leto)** 

## License

This project is licensed under the MIT License
