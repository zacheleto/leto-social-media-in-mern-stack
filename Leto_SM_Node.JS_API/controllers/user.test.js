const chai = require ('chai');
const expect = chai.expect;
const chaiAsPromised = require ('chai-as-promised');
chai.use(chaiAsPromised);
const sinon= require ('sinon');
const sinonChai = require ('sinon-chai');
chai.use(sinonChai);
let mongoose = require ('mongoose');
let users = require ('./user');
let User = require ('../models/user');

describe('users', () => {
    let findStub;
    let sampleArgs;
    let sampleUser;

    beforeEach(()=>{
        sampleUser = {
            id: 123,
            name: 'leto',
            email: 'leto@test.com'
        }

        findStub = sinon.stub(mongoose.Model, 'findById').resolves(sampleUser);
    })
    afterEach(()=>{
        sinon.restore()
    })

    context('get userById', ()=>{
        it('should check for an id', (done)=>{
            users.userById.exec(null, (err, result)=>{
                expect(err).to.exist;
                expect(err.message).to.equal('User not found');
                done();
            })
        })
    })
});
