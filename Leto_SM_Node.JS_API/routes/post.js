const express = require ('express');

const {userById} = require ('../controllers/user');

const {
    getPosts, 
    createPost,
    postsByUser,
    postById,
    deletePost, 
    isPoster,
    updatePost,
    postPhoto,
    singlePost,
    like,
    unlike,
    comment,
    uncomment
    } = require ('../controllers/post');

const {requireSignin} = require ('../controllers/auth');

const validator = require ('../helpers/validator');

const router = express.Router();

router.get('/posts', getPosts);

// like unlike
router.put('/post/like', requireSignin, like);
router.put('/post/unlike', requireSignin, unlike);

// Comments
router.put('/post/comment', requireSignin, comment);
router.put('/post/uncomment', requireSignin, uncomment);

router.post('/post/new/:userId', requireSignin, createPost, validator.createPostValidator);

router.get('/posts/by/:userId', postsByUser);
router.get('/post/:postId', singlePost);

router.get('/post/:postId', singlePost);

router.put('/post/:postId', requireSignin, isPoster, updatePost)

router.delete('/post/:postId', requireSignin, isPoster, deletePost)

//Any route contains userId will first execute userById
router.param('userId', userById)

//Any route contains postId will first execute postById
router.param('postId', postById)

// photo router
router.get('/post/photo/:postId', postPhoto);


module.exports = router;