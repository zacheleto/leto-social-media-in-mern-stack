const express = require ('express');

const router = express.Router();

// const {userUpdateValidator} = require ('../helpers/validator');

const {requireSignin} = require ('../controllers/auth');

const {userById, 
       getAllUsers, 
       getUser,
       updateUser,
       deleteUser,
       hasAuthorization,
       userPhoto,
       addFollowing,
       addFollower,
       removeFollowing,
       removeFollower,
       findPeople
      } = require ('../controllers/user');

const validator = require ('../helpers/validator');

router.put('/user/follow', requireSignin,  addFollowing, addFollower)
router.put('/user/unfollow', requireSignin,  removeFollowing, removeFollower)
router.get('/user/findpeople/:userId', requireSignin, findPeople);


router.get('/users', getAllUsers);

router.get('/user/:userId', requireSignin, getUser);

router.put('/user/:userId', requireSignin, hasAuthorization, updateUser);

router.delete('/user/:userId', requireSignin, hasAuthorization, deleteUser);

//route contains userId will first execute userById
router.param('userId', userById)

// photo router
router.get('/user/photo/:userId', userPhoto);

module.exports = router;