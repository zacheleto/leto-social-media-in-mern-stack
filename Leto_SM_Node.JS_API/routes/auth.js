const express = require ('express');

const router = express.Router();

const { userSignupValidator, passwordResetValidator, userSignUpValidator } = require ('../helpers/validator');

const {signup, 
       signin, 
       signout,
       forgotPassword,
       resetPassword,
       socialLogin
      } = require ('../controllers/auth');


const {userById} = require ('../controllers/user');

//forgot and reset password
router.put("/forgot-password", forgotPassword);
router.put("/reset-password", passwordResetValidator, resetPassword);

// then use this route for social login
router.post("/social-login", socialLogin); 

router.post('/signup', userSignUpValidator ,signup);

router.post('/signin', signin);

router.get('/signout', signout);

//route contains userId will first execute userById
router.param('userId', userById)

module.exports = router;