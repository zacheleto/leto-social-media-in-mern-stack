const mongoose = require('mongoose');
const Post = require("./post");
const { v4: uuidv4 } = require('uuid');
const crypto = require('crypto');
const {ObjectId} = mongoose.Schema;


const userSchema = new mongoose.Schema({
   name: {
    type: String,
    trim: true,
    required: true
   },

   email: {
    type: String,
    trim: true,
    required: true
   },

   hashed_password: {
    type: String,
    required: true
   },

   salt: String,

   created_at: {
    type:Date,
    default: Date.now
   },

   updated_at: Date,

   photo: {
    data: Buffer,
    contentType: String
    },

    about: {
        type: String,
        trim: true
    },

    following: [{ type: ObjectId, ref: "User" }],
    followers: [{ type: ObjectId, ref: "User" }],

    resetPasswordLink: {
        data: String,
        default: ""
    },
    
    role: {
        type: String,
        default: "subscriber"
    }

});

//virtual field
userSchema.virtual('password')
.set(function (password) {
    this._password = password
    this.salt = uuidv4();
    //encryptingpPass
    this.hashed_password = this.encryptPassword(password)
})
.get(function () {
   return this._password
});

//methods
userSchema.methods = {
    authenticate: function(plainText) {
        return this.encryptPassword(plainText) === this.hashed_password;
    },

    encryptPassword: function (password) {
        if (!password) return '';
        try {
            return crypto.createHmac('sha256', this.salt)
            .update(password)
            .digest('hex');
            } catch (err) {
            return '';
            }
    
        }
}   

// pre middleware
userSchema.pre("remove", function(next) {
    Post.remove({ postedBy: this._id }).exec();
    next();
});



module.exports = mongoose.model('User', userSchema);