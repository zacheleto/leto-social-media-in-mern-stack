const express = require ('express');
const app = express();
const mongoose = require ('mongoose');
const morgan = require ('morgan');
const bodyParser = require ('body-parser');
const cookieParser = require('cookie-parser');
const expressValidator = require ('express-validator');
const validator = require ('./helpers/validator');
const env = require ('dotenv');
const fs = require ('fs');
const cors = require('cors');


env.config();

//db
mongoose.connect(process.env.MONGO_URI,  { useNewUrlParser: true, useUnifiedTopology: true })
.then(()=> {console.log('Connected to mongoDB ^__^')})
.catch(err => console.log(err));

app.use(cors());

// connecting to routes
const postRoutes = require ('./routes/post');
const authRoutes = require ('./routes/auth');
const userRoutes = require ('./routes/user');

//apidocs

app.get('/', (req, res, next) => {
  fs.readFile('docs/apiDocs.json', (err, data) => {
      if (err) {
          res.status(400).json({
              error: err
          })
      }
      const docs = JSON.parse(data);
      res.json(docs);
  })
});

app.use(morgan("dev"));

app.use(bodyParser.json());
app.use(cookieParser());

app.use(expressValidator());

app.use('/', postRoutes);
app.use('/', authRoutes);
app.use('/', userRoutes);

// Handling unauthorized error
app.use( (err, req, res, next) => {
    if (err.name === 'UnauthorizedError') {
      res.status(401).json({error: 'You are unauthorized!'});
    }
  });

app.listen(process.env.PORT || 8000);