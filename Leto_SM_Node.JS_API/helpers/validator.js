exports.createPostValidator = (req, res, next) => {
    // checking title errors
    req.check('title', 'Title is required').notEmpty();
    req.check('title', 'Title too short, must be between 4 to 150 characters').isLength({
        min: 4, 
        max: 150
    });

    // checking title errors
    req.check('body', 'Type your content').notEmpty();
    req.check('body', 'Content too short, must be more than 50 characters').isLength({
        min: 50, 
        max: 8000
    });

    // checking title errors
    const errors = req.validationErrors();
    // if errors show the first one as they happen
    if (errors) {
        const firstError = errors.map(error => error.msg)[0];
        return res.status(400).json({error: firstError});
    }
    // proceed to the next middleware
    next();
}


exports.userSignUpValidator = (req, res, next) => {
    // checking name errors
    req.check('name', 'name is required').notEmpty();
    req.check('name', 'name too short').isLength({
        min: 2, 
        max: 20
    });

    // checking email errors
    req.check('email', 'Email is required').notEmpty();
    req.check('email', 'invalid email').matches(/^([a-zA-Z0-9_\-\.]+)@([a-zA-Z0-9_\-\.]+)\.([a-zAZ]{2,6})$/)

    //checking password
    req.check('password', 'password is required').notEmpty();
    req.check('password', 'Password must contain at least one letter, at least one number, and be longer than six charaters.')
    .matches(/^(?=.*[0-9]+.*)(?=.*[a-zA-Z]+.*)[0-9a-zA-Z]{6,15}$/);

    // checking title errors
    const errors = req.validationErrors();
    // if errors show the first one as they happen
    if (errors) {
        const firstError = errors.map(error => error.msg)[0];
        return res.status(400).json({error: firstError});
    }
    // proceed to the next middleware
    next();
}

exports.userUpdateValidator = (req, res, next) => {
    // checking name errors
    req.check('name', 'name is required').notEmpty();
    req.check('name', 'name too short').isLength({
        min: 2, 
        max: 20
    });

    // checking email errors
    req.check('email', 'Email is required').notEmpty();
    req.check('email', 'invalid email').matches(/^([a-zA-Z0-9_\-\.]+)@([a-zA-Z0-9_\-\.]+)\.([a-zAZ]{2,6})$/)

    //checking password
    req.check('password', 'password is required').notEmpty();
    req.check('password', 'Password must contain at least one letter, at least one number, and be longer than six charaters.')
    .matches(/^(?=.*[0-9]+.*)(?=.*[a-zA-Z]+.*)[0-9a-zA-Z]{6,15}$/);

    // checking title errors
    const errors = req.validationErrors();
    // if errors show the first one as they happen
    if (errors) {
        const firstError = errors.map(error => error.msg)[0];
        return res.status(400).json({error: firstError});
    }
    // proceed to the next middleware
    next();

    exports.passwordResetValidator = (req, res, next) => {
    // check for password
    req.check("newPassword", "Password is required").notEmpty();
    req.check("newPassword")
        .isLength({ min: 6 })
        .withMessage("Password must be at least 6 chars long")
        .matches(/\d/)
        .withMessage("must contain a number")
        .withMessage("Password must contain a number");
 
    // check for errors
    const errors = req.validationErrors();
    // if error show the first one as they happen
    if (errors) {
        const firstError = errors.map(error => error.msg)[0];
        return res.status(400).json({ error: firstError });
    }
    // proceed to next middleware or ...
    next();
    };

}

exports.passwordResetValidator = (req, res, next) => {
    // check for password
    req.check("newPassword", "Password is required").notEmpty();
    req.check("newPassword")
        .isLength({ min: 6 })
        .withMessage("Password must be at least 6 chars long")
        .matches(/\d/)
        .withMessage("must contain a number")
        .withMessage("Password must contain a number");
 
    // check for errors
    const errors = req.validationErrors();
    // if error show the first one as they happen
    if (errors) {
        const firstError = errors.map(error => error.msg)[0];
        return res.status(400).json({ error: firstError });
    }
    // proceed to next middleware or ...
    next();
};

