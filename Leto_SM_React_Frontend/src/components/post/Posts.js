import React, { Component } from 'react'
import { list } from './apiPost'
import { Link } from 'react-router-dom';
import defaultAvatar from '../img/postavatar.jpg'
import loadingGif from '../img/postsloading.gif';

export default class Posts extends Component {
    constructor(){
        super()
        this.state = {
            posts: [],
            loading: false,
            page: 1
        }
    }

    componentDidMount() {

        this.loadPosts(this.state.page);
    }

    loadPosts = page => {
        list(page)
        .then(data => {
            if (data.error) {
                console.log(data.error)
            } else {
                this.setState({
                    posts: data,
                    loading: true
                })
            }
        })
    }

    loadMore = number => {
        this.setState({ page: this.state.page + number });
        this.loadPosts(this.state.page + number);
    };
 
    loadLess = number => {
        this.setState({ page: this.state.page - number });
        this.loadPosts(this.state.page - number);
    };


    renderPosts = posts => {
        return (
            
            <div className="row mt-4">
           {posts.map((post, i) => {

                const posterId = post.postedBy ? post.postedBy._id : '' ;
                const posterName = post.postedBy ? post.postedBy.name : '' ;
                return(
                <div className="card col-md-4" key={i}>
                <img src={`${process.env.REACT_APP_API_URL}/post/photo/${post._id}?${new Date().getTime()}`} onError={i => (i.target.src=`${defaultAvatar}`)} className="card-img-top mt-3" style={{height:'250px'}} alt="post"/>
                <div className="card-body">
                    <h5 className="card-title">{post.title}</h5>
                    <p className="card-text">{post.body && post.body.substring(0,120)}<Link className="text-info" to={`/post/${post._id}`}>
                        {post.body && ( post.body.length <= 120 ? '' : '...readMore')}</Link></p>
                </div>
                <ul className="list-group list-group-flush">
                    <li className="list-group-item">Posted on: {new Date(post.created_at).toDateString()}</li>
                </ul>
                <div className="card-body">
                    <Link to={`/user/${posterId}`} className="card-link">Created by:<span className="text-info ml-2"> {posterName} </span></Link>
                </div>
                </div>
                )
      
            })}
        </div>
        
        );

    }

    render() {
        const {posts, loading, page} = this.state;

        return (
            <div className="container">
                
                
                {loading ? (
                    
                    <h2 className="mt-4 card-header text-center">Recent Posts</h2>
                    
                    ) : 
                    
                    <div className="text-center"><img src={loadingGif} alt="loading gif" height="260px" width="260px"></img></div>
                 }
                {this.renderPosts(posts)}

                {page > 1 ? (
                    <span
                        className="badge badge-light mr-2 mt-5 mb-5"
                        onClick={() => this.loadLess(1)}
                        style={{cursor:'pointer'}}
                    >
                        <h3>Previous</h3> 
                        {/* ({this.state.page - 1}) */}
                    </span>
                ) : (
                    ""
                )}
 
                {posts.length ? (
                    <span
                        className="badge badge-light mt-5 mb-5 "
                        onClick={() => this.loadMore(1)}
                        style={{cursor:'pointer'}}
                    >
                        <h3>Next</h3> 
                        {/* ({page + 1}) */}
                    </span>
                ) : (
                    ""
                )}
                     
                </div>
        )
    }
}
