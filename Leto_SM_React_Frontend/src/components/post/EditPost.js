import React, { Component } from 'react'
import { singlePost } from './apiPost'
import { Redirect } from 'react-router-dom'
import loadingGif from '../img/loading.gif';
import { update } from './apiPost'
import {isAuthenticated} from '../auth/auth';
import defaultAvatar from '../img/postavatar.jpg';

export default class EditPost extends Component {
    constructor() {
        super();
        this.state = {
            id: "",
            title: "",
            body: "",
            redirectToProfile: false,
            error: "",
            fileSize: 0,
            loading: false
        };
    }

    init = postId => {
        singlePost(postId).then(data => {
            if (data.error) {
                this.setState({ redirectToProfile: true });
            } else {
                this.setState({
                    id: data.postedBy._id,
                    title: data.title,
                    body: data.body,
                    error: ""
                });
            }
        });
    };

    componentDidMount() {
        this.postData = new FormData();
        const postId = this.props.match.params.postId;
        this.init(postId);
    }

    isValid = () => {
        const { title, body, fileSize } = this.state;
        if (fileSize > 1000000) {
            this.setState({
                error: "File size should be less than 100kb",
                loading: false
            });
            return false;
        }
        if (title.length === 0 || body.length === 0) {
            this.setState({ error: "All fields are required", loading: false });
            return false;
        }
        return true;
    };

    handleChange = name => event => {
        this.setState({ error: "" });
        const value =
            name === "photo" ? event.target.files[0] : event.target.value;

        const fileSize = name === "photo" ? event.target.files[0].size : 0;
        this.postData.set(name, value);
        this.setState({ [name]: value, fileSize });
    };

    clickSubmit = event => {
        event.preventDefault();
        this.setState({ loading: true });

        if (this.isValid()) {
            const postId = this.props.match.params.postId;
            const token = isAuthenticated().token;

            update(postId, token, this.postData).then(data => {
                if (data.error) this.setState({ error: data.error });
                else {
                    this.setState({
                        loading: false,
                        title: "",
                        body: "",
                        redirectToProfile: true
                    });
                }
            });
        }
    };





    render() {
        const {id, redirectToProfile, error, loading, title, body} = this.state
        const postId = this.props.match.params.postId;
        if (redirectToProfile) {
            return <Redirect to={`/post/${postId}`} />
        }

        const photoUrl = id ? `${process.env.REACT_APP_API_URL}/post/photo/${id}?${new Date().getTime()}` : `${defaultAvatar}`

        return (
            <div className="container">
                {(isAuthenticated().user.role === "admin" || isAuthenticated().user._id === id) &&

            <div className="row mt-4">
                    <div className="col-md-4">

                        <img src={photoUrl} onError={i => (i.target.src=`${defaultAvatar}`)} className="card-body card-img-top mt-1 image-border" alt={title}/>
                        
                        <input onChange={this.handleChange('photo')}  type="file" accept='image/*' className="form-control-file mb-3"></input>
                    </div>                       

                <div className="col-md-8">
                <div className="card">
                    <h1 className="card-header">Edit Post</h1>
                    <div id="alert" className="alert alert-info text-center" style={{display: error ? '' : 'none'}}>{error}</div>
                    {loading ? (
                    
                    <div className="text-center"><img src={loadingGif} alt="loading gif" height="260px" width="200px"></img></div>
                    
                    ) : 
                    <form className="card-body">
                        <div className="form-groupe">
                            
                            <input onChange={this.handleChange('title')} type="text" className="form-control" placeholder="Write a title" value={title}/>
                        </div>
                        <hr className="bg-info"/>
                        <div className="form-groupe">
                            <textarea onChange={this.handleChange('body')} type="text" className="form-control"  placeholder="Write your content" value={body}/>
                        </div>
                        <hr className="bg-info"/>
                        <button onClick={this.clickSubmit} className="badge badge-secondary btn-block">
                        <h5>Save Changes</h5>
                        </button>
                    </form>
                    }
                            
                    <div>
                        
                        
                    </div>
                </div>
                </div>  
            </div>
    
           }
        </div>
        )
    }
}
