import React, { Component } from "react";
import defaultAvatar from '../img/postavatar.jpg'
import { singlePost, remove, like, unlike } from './apiPost'
import { Link, Redirect } from "react-router-dom";
import loadingGif from '../img/loading.gif'
import { isAuthenticated } from "../auth/auth";
import Comment from './Comment'

export default class SinglePost extends Component {
                 state = {
                   post: "",
                   loading: false,
                   redirectToHome: false,
                   redirectToLogin: false,
                   like: false,
                   likes: 0,
                   comments: [],
                 };

                 checkLike = (likes) => {
                   const userId =
                     isAuthenticated() && isAuthenticated().user._id;
                   let match = likes.indexOf(userId) !== -1;
                   return match;
                 };

                 componentDidMount = () => {
                   const postId = this.props.match.params.postId;
                   singlePost(postId).then((data) => {
                     if (data.error) {
                       console.log(data.error);
                     } else {
                       this.setState({
                         post: data,
                         likes: data.likes.length,
                         like: this.checkLike(data.likes),
                         comments: data.comments,
                       });
                     }
                   });
                 };

                 updateComments = (comments) => {
                   this.setState({ comments });
                 };

                 likeToggle = () => {
                   if (!isAuthenticated()) {
                     this.setState({ redirectToLogin: true });
                     return false;
                   } else {
                     let callApi = this.state.like ? unlike : like;
                     const userId = isAuthenticated().user._id;
                     const postId = this.state.post._id;
                     const token = isAuthenticated().token;

                     callApi(userId, token, postId).then((data) => {
                       if (data.error) {
                         console.log(data.error);
                       } else {
                         this.setState({
                           like: !this.state.like,
                           likes: data.likes.length,
                         });
                       }
                     });
                   }
                 };

                 deletePost = () => {
                   const postId = this.props.match.params.postId;
                   const token = isAuthenticated().token;
                   remove(postId, token).then((data) => {
                     if (data.error) {
                       console.log(data.error);
                     } else {
                       this.setState({ redirectToHome: true });
                     }
                   });
                 };

                 deleteConfirmed = () => {
                   let answer = window.confirm(
                     "Are you sure you want to delete This Post?"
                   );
                   if (answer) {
                     this.deletePost();
                   }
                 };

                 renderPost = (post) => {
                   const posterId = post.postedBy
                     ? `/user/${post.postedBy._id}`
                     : "";
                   const posterName = post.postedBy
                     ? post.postedBy.name
                     : " Unknown";

                   const { like, likes } = this.state;

                   return (
                     <div className="col-md-12 mx-auto mt-4 card">
                       <h1 className="text-center mb-4 mt-4 card-header">
                         {post.title}
                       </h1>

                       {isAuthenticated().user &&
                         isAuthenticated().user._id === post.postedBy._id &&
                         isAuthenticated().user.role === "subscriber" && (
                           <div className="d-inline p-3 text-right">
                             <Link
                               className="badge badge-secondary mr-2"
                               to={`/post/edit/${post._id}`}
                             >
                               <h5>Edit Post</h5>
                             </Link>
                             <span
                               className="badge badge-danger mr-2"
                               onClick={this.deleteConfirmed}
                               style={{cursor:'pointer'}}
                             >
                               <h5>Remove Post</h5>
                             </span>
                           </div>
                         )}

                        {isAuthenticated().user &&
                         isAuthenticated().user.role === "admin" && (
                           <div className="d-inline p-3 text-right">
                             <h4 className="mb-2 text-danger text-center card-header">
                          Edit/Delete as an Admin
                          </h4>
                             <Link
                               className="badge badge-secondary mr-2"
                               to={`/post/edit/${post._id}`}
                             >
                               <h5>Edit Post</h5>
                             </Link>
                             <span
                               className="badge badge-danger mr-2"
                               onClick={this.deleteConfirmed}
                               style={{cursor:'pointer'}}
                             >
                               <h5>Remove Post</h5>
                             </span>
                           </div>
                         )}

                       <div className="card-body">
                         <img
                           src={`${process.env.REACT_APP_API_URL}/post/photo/${
                             post._id
                           }?${new Date().getTime()}`}
                           onError={(i) => (i.target.src = `${defaultAvatar}`)}
                           className="card-img-top mt-3"
                           alt="post"
                         />
                         <hr className="bg-info" />
                         <div className="col-md-2">
                           {like ? (
                             <h3 onClick={this.likeToggle}>
                               <i
                                 className="fa fa-thumbs-up text-info bg-dark mr-2"
                                 style={{
                                   padding: "10px",
                                   borderRadius: "50%",
                                   cursor:'pointer'
                                 }}
                                 
                               ></i>
                               {likes} Like
                             </h3>
                           ) : (
                             <h3 onClick={this.likeToggle}>
                               <i
                                 className="fa fa-thumbs-up bg-dark mr-2"
                                 style={{
                                   padding: "10px",
                                   borderRadius: "50%",
                                   cursor:'pointer'
                                 }}
                               ></i>
                               {likes} Like
                             </h3>
                           )}
                         </div>
                         <hr className="bg-info" />
                         <div className="card-body">
                           <h5 className="card p-4">{post.body}</h5>

                           <div className="card">
                             <ul className="list-group list-group-flush">
                               <li className="list-group-item disabled text-white">
                                 Posted on:{" "}
                                 {new Date(post.created_at).toDateString()}
                               </li>
                             </ul>
                             <div className="card-body">
                               <Link to={`${posterId}`} className="card-link">
                                 Created by:
                                 <span className="text-info ml-2">
                                   {" "}
                                   {posterName}{" "}
                                 </span>
                               </Link>
                             </div>
                           </div>
                         </div>
                       </div>
                     </div>
                   );
                 };

                 render() {
                   const {
                     post,
                     redirectToHome,
                     redirectToLogin,
                     comments,
                   } = this.state;

                   if (redirectToHome) {
                     return <Redirect to={`/`} />;
                   }

                   if (redirectToLogin) {
                     return <Redirect to={`/signin`} />;
                   }
                   return (
                     <div className="container">
                       <div className="row mt-4 mb-4">
                         {!post ? (
                           <div className="col-md-4 mx-auto mt-4">
                             <img
                               src={loadingGif}
                               className="img-fluid"
                               alt="loading gif"
                             ></img>
                           </div>
                         ) : (
                           this.renderPost(post)
                         )}
                       </div>
                      
                       <Comment
                         postId={post._id}
                         comments={comments}
                         updateComments={this.updateComments}
                       />
                       
                     </div>
                   );
                 }
               }
