import React, { Component } from 'react'
import {isAuthenticated} from '../auth/auth';
import {create} from './apiPost';
import { Redirect } from 'react-router-dom';
import loadingGif from '../img/loading.gif';

export default class NewPost extends Component {
    constructor(){
        super()
        this.state = {
            title: '',
            body: '',
            photo: '',
            error: '',
            user: {},
            fileSize: 0,
            loading: false,
            redirectToProfile: false

        }
    }


    componentDidMount() {
        this.postData = new FormData()
        this.setState({user: isAuthenticated().user})

    }

    isValid = () => {
        const {title, body, fileSize } = this.state;
        if (fileSize > 1000000) {
          this.setState({
            error: "File size should be less than 1mb",
            loading: false
          });
          return false;
        }
        if (title.length === 0 || body.length === 0) {
          this.setState({ error: "All fields are required", 
          loading: false 
            });
          return false;
        }
        
        return true;
      };

      handleChange = name => event => {
        this.setState({ error: "" });
        const value =
            name === "photo" ? event.target.files[0] : event.target.value;

        const fileSize = name === "photo" ? event.target.files[0].size : 0;
        this.postData.set(name, value);
        this.setState({ [name]: value, fileSize });
    };

    clickSubmit = event => {
        event.preventDefault();
        this.setState({ loading: true });

        if (this.isValid()) {
            const userId = isAuthenticated().user._id;
            const token = isAuthenticated().token;

            create(userId, token, this.postData).then(data => {
                if (data.error) this.setState({ error: data.error });
                else {
                    console.log(data)
                    this.setState({
                        loading: false,
                        title: "",
                        body: "",
                        redirectToProfile: true
                    });
                }
            });
        }
    };
        
    render() {
        const {title, body, user, error, loading, redirectToProfile} = this.state;
        if (redirectToProfile) {
            return <Redirect to={`/user/${user._id}`} />
        }
        // const photoUrl = id ? `${process.env.REACT_APP_API_URL}/user/photo/${id}?${new Date().getTime()}` : `${defaultAvatar}`

        return (
            <div className="container">
            <div className="row mt-4">
              
                    <div className="col-md-4">
                        {/* <img src={photoUrl} onError={i => (i.target.src=`${defaultAvatar}`)} className="card-body card-img-top mt-1 image-border" alt={name}/> */}
                        
                        <input onChange={this.handleChange('photo')}  type="file" accept='image/*' className="form-control-file mb-3"></input>
                    </div>                       

                <div className="col-md-8">
                <div className="card">
                    <h1 className="card-header">Create a new Post</h1>
                    <div id="alert" className="alert alert-info text-center" style={{display: error ? '' : 'none'}}>{error}</div>
                    {loading ? (
                    
                    <div className="text-center"><img src={loadingGif} alt="loading gif" height="260px" width="200px"></img></div>
                    
                    ) : 
                    <form className="card-body">
                        <div className="form-groupe">
                            
                            <input onChange={this.handleChange('title')} type="text" className="form-control" placeholder="Write a title" value={title}/>
                        </div>
                        <hr className="bg-info"/>
                        <div className="form-groupe">
                            <textarea onChange={this.handleChange('body')} type="text" className="form-control"  placeholder="Write your content" value={body}/>
                        </div>
                        <hr className="bg-info"/>
                        <button onClick={this.clickSubmit} className="badge badge-secondary btn-block">
                        <h5>Create Post</h5>
                        </button>
                    </form>
                    }
                    <div>
                        
                        
                    </div>
                </div>
                </div>  
            </div>
                




        </div>
        )
    }
}
