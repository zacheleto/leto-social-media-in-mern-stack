import React, { Component } from 'react'
import { isAuthenticated } from '../auth/auth'
import { comment, uncomment } from './apiPost'
import defaultAvatar from '../img/avatar.jpg'
import { Link } from 'react-router-dom'

export default class Comment extends Component {

    state= {
        text: '',
        error: ''
    }

    handleChange = e => {
      this.setState({error: ''})
        this.setState({ text: e.target.value });
    }

    isValid = () => {
      const { text } = this.state;
      if (!text.length > 0 || text.length > 1000) {
          this.setState({
              error:
                  "Comment should not be empty and less than 1000 characters long"
          });
          return false;
      }
      return true;
  };

    addComment = (e) => {
        e.preventDefault();

        if (!isAuthenticated()) {
          this.setState({
            error: 'Only members can leave a comment'
          })
          return false;
        }

        if (this.isValid()) {
        const userId = isAuthenticated().user._id
        const token = isAuthenticated().token
        const postId = this.props.postId
        const _comment = {text: this.state.text}

        comment(userId, token, postId, _comment)
        .then(data => {
            if (data.error) {
                console.log(data.error);
            } else {
                this.setState({
                  text: ''
                });
                this.props.updateComments(data.comments);
            }
        });
        }

    }

    deleteComment = (comment) => {
      const userId = isAuthenticated().user._id;
        const token = isAuthenticated().token;
        const postId = this.props.postId;

        uncomment(userId, token, postId, comment).then(data => {
            if (data.error) {
                console.log(data.error);
            } else {
                this.props.updateComments(data.comments);
            }
        });
    };

    deleteConfirmed = (comment) => {
      let answer = window.confirm(
        "Are you sure you want to delete This Comment?"
      );
      if (answer) {
        this.deleteComment(comment);
      }
    };


    render() {
        const {comments} = this.props
        const {error} = this.state
        return (
            <div>
                <div className="col-md-12">
                <h3 className="card-header">{comments.length} Comments</h3>
                <div className=" mb-3 mt-3">
                {comments.reverse().map((comment, i) => {
                  return (
                    
                    <ul key={i} className="card-header border-info mb-3">
                      <Link to={`/user/${comment.postedBy._id}`}>
                        <li className="list-group-item">
                        <img
                            style={{ height: "50px", width: "50px" }}
                            className="mr-3"
                            onError={(i) => (i.target.src = `${defaultAvatar}`)}
                            src={`${process.env.REACT_APP_API_URL}/user/photo/${
                              comment.postedBy._id
                            }?${new Date().getTime()}`}
                            alt={comment.postedBy.name}
                          />
                          {comment.postedBy.name}
                        </li>
                        
                      </Link>
                      <li className="list-group-item disabled text-white">
                          Posted on: {new Date(comment.created_at).toDateString()}
                        </li>
                        
                        {isAuthenticated().user &&
                         isAuthenticated().user._id === comment.postedBy._id && (
                         
                             <span
                               className="badge badge-danger float-right mr-2"
                               style={{cursor:'pointer'}}
                               onClick={()=> this.deleteConfirmed(comment)}
                             >
                               <h5>Remove</h5>
                             </span>
                          
                         )}
                      <h5 className="card p-4">{comment.text}</h5>
                    </ul>
                  ); 
                })}
              </div>
              </div>

            
            <div>
            
                <h2 className="mt-4 card-header">Leave a comment</h2>
                <div id="alert" className="alert alert-danger text-center" style={{display: error ? '' : 'none'}}>{error}</div>
                <form onSubmit={this.addComment}>
                <textarea className="card col-md-12 mx-auto text-white" style={{height:"200px"}} type="text" onChange={this.handleChange} value={this.state.text} />
                <button className="btn btn-secondary btn-block">Post your comment</button>
                </form>
                {/* {JSON.stringify(comments)} */}
            </div>
            </div>
        )
    }
}
