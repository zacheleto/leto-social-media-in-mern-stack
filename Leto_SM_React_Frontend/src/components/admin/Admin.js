import React, { Component } from 'react'
import Posts from "../post/Posts";
import Users from '../user/Users';
import { isAuthenticated } from "../auth/auth";
import { Redirect } from "react-router-dom";

export default class Admin extends Component {

    state = {
        redirectToHome: false
    };

    componentDidMount() {
        if (isAuthenticated().user.role !== "admin") {
            this.setState({ redirectToHome: true });
        }
    }

    render() {
        
        if (this.state.redirectToHome) {
            return <Redirect to="/" />;
        }

        return (
            <div className="container">
                <div className="jumbotron">
                    <h2>Admin Dashboard</h2>
                    <p className="lead">Welcome to Leto Social Media Frontend with React</p>
                </div>
                <div className="container-fluid">
                    <div className="row">
                    <ul className="nav nav-tabs">
                        
                        <li className="nav-item">
                            
                            <a className="nav-link" data-toggle="tab" href="#posts">Posts</a>
                        </li>
                        <li className="nav-item">
                            <a className="nav-link" data-toggle="tab" href="#users">Users</a>
                        </li>
                    </ul>
                    <div id="myTabContent" class="tab-content">
                        <div class="tab-pane fade" id="posts">
                            <Posts />
                        </div>
                        <div class="tab-pane fade" id="users">
                            <Users />
                        </div>
                    </div>
                        
                    </div>
                </div>
            </div>
        );
    }
}
