import React, { Component } from 'react'
import { list } from './apiUser'
import { Link } from 'react-router-dom';
import defaultAvatar from '../img/avatar.jpg'

export default class Users extends Component {
    constructor(){
        super()
        this.state = {
            users: []
        }
    }

    componentDidMount() {
        list()
        .then(data => {
            if (data.error) {
                console.log(data.error)
            } else {
                this.setState({users: data})
            }
        })
    }

    renderUsers = users => {

        return <div>
        <div className="row mt-4">
        {users.map((user, i) => (
            <div className="card col-md-3" key={i}>
            <img src={`${process.env.REACT_APP_API_URL}/user/photo/${user._id}?${new Date().getTime()}`} onError={i => (i.target.src=`${defaultAvatar}`)} className="card-img-top mt-3" style={{height:'250px'}} alt="profile"/>
            <div className="card-body">
                <h5 className="card-title">{user.name}</h5>
                <p className="card-text">{user.about && user.about.substring(0,120)}<Link className="text-info" to={`/user/${user._id}`}>
                    {user.about && ( user.about.length <= 120 ? '' : '...readMore')}</Link></p>
            </div>
            <ul className="list-group list-group-flush">
                <li className="list-group-item text-info">{user.email}</li>
                <li className="list-group-item">{new Date(user.created_at).toDateString()}</li>
            </ul>
            <div className="card-body">
                <Link to={`/user/${user._id}`} className="card-link">View Profile</Link>
            </div>
            </div>
      

           
        ))}
    </div>
    </div>
    }

    render() {
        const {users} = this.state;
        return (
            <div className="container">
                
                <h2 className="mt-4 card-header text-center">Hi there! It's great to have you aboard</h2>
                
                {this.renderUsers(users)}

                </div>
        )
    }
}
