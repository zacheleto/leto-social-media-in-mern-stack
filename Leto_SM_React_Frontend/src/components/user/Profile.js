import React, { Component } from 'react'
import {isAuthenticated} from '../auth/auth'
import { Redirect, Link } from 'react-router-dom';
import {getUserData} from './apiUser';
import defaultAvatar from '../img/avatar.jpg'
import DeleteUser from './DeleteUser';
import FollowProfileButton from './FollowProfileButton';
import ProfileTabs from './ProfileTabs';
import { listByUser } from '../post/apiPost';

export default class Profile extends Component {
    constructor(){
        super()
        this.state = {
            user: {following: [], followers: []},
            redirectToSignIn: false,
            following: false,
            error: '',
            posts: []
        }
    }


    init = userId => {
        const token = isAuthenticated().token;
        getUserData(userId, token).then(data => {
          if (data.error) {
            this.setState({ redirectToSignin: true });
          } else {
            let following = this.checkFollow(data);
            this.setState({ user: data, following });
            this.loadPosts(data._id)
          }
        });
      };

    loadPosts = userId => {
      const token = isAuthenticated().token;
      listByUser(userId, token)
      .then(data => {
        if (data.error) {
          console.log(data.error);
        } else {
          this.setState({posts: data});
        }
      })
    }

  // check follow
  checkFollow = user => {
    const jwt = isAuthenticated();
    const match = user.followers.find(follower => {
      // one id has many other ids (followers) and vice versa
      return follower._id === jwt.user._id;
    });
    return match;
  };

  clickFollowButton = apiCall => {
    const userId = isAuthenticated().user._id;
    const token = isAuthenticated().token;

    apiCall(userId, token, this.state.user._id).then(data => {
      if (data.error) {
        this.setState({ error: data.error });
      } else {
        this.setState({ user: data, following: !this.state.following });
      }
    });
  };

    componentDidMount() {
        const userId = this.props.match.params.userId;
        this.init(userId);
    }

    componentWillReceiveProps(props) {
        const userId = props.match.params.userId;
        this.init(userId);
    }


    render() {
        const {redirectToSignIn, user, following, posts} = this.state;
        if (redirectToSignIn) {

            return <Redirect to="/signin"/>
        }
        const photoUrl = user._id ? `${process.env.REACT_APP_API_URL}/user/photo/${user._id}?${new Date().getTime()}` : `${defaultAvatar}`
        return (
            <div className="container">
                <div className="row mt-4">

                        <div className="col-md-4" >
                      <img src={photoUrl} onError={i => (i.target.src=`${defaultAvatar}`)} className="card-body card-img-top mt-1 image-border" alt="profile"/>
                     {isAuthenticated().user && isAuthenticated().user._id !== user._id && (
                        <FollowProfileButton following={following} onButtonClick={this.clickFollowButton} />
                        )}

                        </div> 

                    <div className="col-md-8">
                    <div className="card">
                        <h1 className="card-header">Profile details</h1>
                        {isAuthenticated().user && isAuthenticated().user._id === user._id && isAuthenticated().user 
                        && isAuthenticated().user.role === "subscriber" && (
                                <div className="d-inline p-3 text-right">
                                    <Link className="badge badge-info float-left" to={'/post/create'}><h5>Create Post</h5></Link>
                                   <Link className="badge badge-secondary mr-2" to={`/user/edit/${user._id}`}><h5>Edit Profile</h5></Link>
                                   <DeleteUser userId={user._id} />
                                   
                                </div>
                        )}
                        
                      {isAuthenticated().user && isAuthenticated().user.role === "admin" && (
                          <div className="d-inline p-3 text-right">
                          <h4 className="mb-2 text-danger text-center card-header">
                          Edit/Delete as an Admin
                          </h4>
                                <Link  className="badge badge-secondary mr-2" to={`/user/edit/${user._id}`}>
                                <h5>Edit Profile</h5>
                                </Link>
                                <DeleteUser userId={user._id} />
                          </div>
                          )}
                            <div className="p-3">
                                <hr className="bg-info"/>
                                <h5>Name: {user.name} </h5>
                                <hr className="bg-info"/>
                                <h5>Email: {user.email} </h5>
                                <hr className="bg-info"/>
                                <h5>{`Joined: ${new Date(user.created_at).toDateString()}`} </h5>
                                <hr className="bg-info"/>
                                <label><h5>About:</h5></label>
                                <p className="text-justify"> {user.about} </p>
                                <hr className="bg-info"/>
                            
                            </div>
                        <div>
                            
                            
                        </div>
                    </div>
                    </div>  
                </div>
              
                          <ProfileTabs followers={user.followers} following={user.following} posts={posts} />
                     
            </div>
        )
    }
}
