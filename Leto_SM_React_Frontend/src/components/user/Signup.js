import React, { Component } from 'react';
import {Link} from 'react-router-dom';
import { signUp } from '../auth/auth';

export default class Signup extends Component {
    constructor() {
        super()
        this.state = {
            name: '',
            email: '',
            password: '',
            error: '',
            message:'',
        }
        
    }

    handleChange = name => event => {
        this.setState({error: ''});
        
        this.setState({[name]: event.target.value});
    };

    signupUser = e => {

        e.preventDefault()
        const {name, email, password} = this.state
        const user = {
                        name, 
                        email,
                        password
                     };

        signUp(user)
        .then(result => {
            if (result.error) {
                this.setState({error: result.error})
            } else {
                this.setState({
                    error:'',
                    name:'',
                    email:'',
                    password:'',
                    message: result.message,
                    
                })
            }
        })
                     
    };

    signUpForm = (name, email, password) => (
        <form className="card-body">
        <div className="form-groupe">
             <label>Name</label>
             <input onChange={this.handleChange('name')} type="text" className="form-control" value={name}/>
         </div>
         <br/>
         <div className="form-groupe">
             <label>Email</label>
             <input onChange={this.handleChange('email')} type="text" className="form-control" value={email}/>
         </div>
         <br/>
         <label>Password</label>
         <div className="form-groupe">
             <input onChange={this.handleChange('password')} type="password" className="form-control" value={password}/>
         </div>
         {/* <br/>
         <div className="form-groupe">
             <input onChange={this.handleChange('repeat Password')} type="password" className="form-control" placeholder="repeat password"/>
         </div> */}
         <hr/>
         <button onClick={this.signupUser} className="btn btn-secondary">Signup</button>
       </form>
    );


    render() {
        const {name, email, password, error, message} = this.state;
        return (
            <div className="container mt-4">
                <div className="col-md-6 mx-auto mt-4 card">
                <h1 className="text-center mb-4 mt-4 card-header">It's great to have you aboard ツ</h1>
                    <div id="alert" className="alert alert-info text-center" style={{display: error ? '' : 'none'}}>{error}</div>
                    <div className="alert alert-success text-center" style={{display: message  ? '' : 'none'}}><h5><Link to="/signin">{message}</Link></h5></div>
                  { !message ? this.signUpForm(name, email, password) : ''}
                </div>
            </div>
        );
    }
}
