import React, { Component } from 'react'
import {isAuthenticated} from '../auth/auth';
import {getUserData, update, updateUser} from './apiUser';
import defaultAvatar from '../img/avatar.jpg';
import { Redirect } from 'react-router-dom';
import loadingGif from '../img/loading.gif';

export default class EditProfile extends Component {
    constructor(){
        super()
        this.state = {
            id: '',
            name: '',
            email: '',
            password: '',
            redirectToProfile: false,
            error: '',
            fileSize: 0, 
            loading: false,
            about: ''
        }
    }


    init = userId => {
        const token = isAuthenticated().token;
        getUserData(userId, token)
        
        .then(data => {
            if (data.error) {
                this.setState({redirectToProfile: true})
            } else {
                this.setState({
                    id: data._id,
                    name: data.name,
                    email: data.email,
                    about: data.about,
                    error: ''
                });
            }
        })
    }

    componentDidMount() {
        this.userData = new FormData()
        const userId = this.props.match.params.userId;
        this.init(userId);
    }

    isValid = () => {
        const { name, email, password, fileSize } = this.state;
        if (fileSize > 1000000) {
          this.setState({
            error: "File size should be less than 1mb",
            loading: false
          });
          return false;
        }
        if (name.length === 0) {
          this.setState({ error: "Name is required", 
          loading: false 
            });
          return false;
        }
        // email@domain.com
        // eslint-disable-next-line
        if (!/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(email)) {
          this.setState({
            error: "A valid Email is required",
            loading: false
          });
          return false;
        }
        if (password.length >= 1 && password.length <= 5) {
          this.setState({
            error: "Password must be at least 6 characters long",
            loading: false
          });
          return false;
        }
        return true;
      };

      handleChange = name => event => {
        this.setState({ error: "" });
        const value = name === "photo" ? event.target.files[0] : event.target.value;
    
        const fileSize = name === "photo" ? event.target.files[0].size : 0;
        this.userData.set(name, value);
        this.setState({ [name]: value, fileSize });
      };

      clickSubmit = event => {
        event.preventDefault();
        this.setState({ loading: true });
    
        if (this.isValid()) {
          const userId = this.props.match.params.userId;
          const token = isAuthenticated().token;
    
          update(userId, token, this.userData).then(data => {
            if (data.error) {
              this.setState({ error: data.error });
            } else if (isAuthenticated().user.role === "admin") {
              this.setState({
                redirectToProfile: true
              });
            } else {
              updateUser(data, () => {
                this.setState({
                  redirectToProfile: true
                });
              });
            }
          });
        }
      };
        
    render() {
        const {id, name, email, password, about, redirectToProfile, error, loading} = this.state;
        if (redirectToProfile) {
            return <Redirect to={`/user/${id}`} />
        }
        const photoUrl = id ? `${process.env.REACT_APP_API_URL}/user/photo/${id}?${new Date().getTime()}` : `${defaultAvatar}`

        return (
           
            <div className="container">
            {(isAuthenticated().user.role === "admin" || isAuthenticated().user._id === id) && 
            
            <div className="row mt-4">
              
                    <div className="col-md-4">
                    
                        <img src={photoUrl} onError={i => (i.target.src=`${defaultAvatar}`)} className="card-body card-img-top mt-1 image-border" alt={name}/>
                        <input onChange={this.handleChange('photo')}  type="file" accept='image/*' className="form-control-file mb-3"></input>
                        
                    </div>                       

                <div className="col-md-8">
                <div className="card">
                    <h1 className="card-header">Update Profile</h1>
                    <div id="alert" className="alert alert-info text-center" style={{display: error ? '' : 'none'}}>{error}</div>
                   
                    
                    {loading ? (
                    
                    <div className="text-center"><img src={loadingGif} alt="loading gif" height="260px" width="200px"></img></div>
                    
                    ) : 
                    
                    <form className="card-body">
                        <div className="form-groupe">
                            
                            <input onChange={this.handleChange('name')} type="text" className="form-control" placeholder="update your name" value={name}/>
                        </div>
                         <hr className="bg-info"/>
                        <div className="form-groupe">
                            
                            <input disabled onChange={this.handleChange('email')} type="text" className="form-control " placeholder="update your email"value={email}/>
                        </div>
                         <hr className="bg-info"/>
                        
                        <div className="form-groupe">
                            <input onChange={this.handleChange('password')} type="password" className="form-control"  placeholder="update your password" value={password}/>
                        </div>
                        <hr className="bg-info"/>
                        <div className="form-groupe">
                            <textarea onChange={this.handleChange('about')} type="text" className="form-control"  placeholder="Write something about your self..." value={about}/>
                        </div>
                        <hr className="bg-info"/>
                        <button onClick={this.clickSubmit} className="badge badge-secondary btn-block">
                        <h5>Save changes</h5>
                        </button>
                    </form>
                    
                    }
                    <div>
                         
                        
                    </div>
                </div>
                </div>  
            </div>
                



            
            }
        </div>
        )
    }
}
