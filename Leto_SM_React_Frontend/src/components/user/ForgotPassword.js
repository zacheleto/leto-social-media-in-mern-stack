import React, { Component } from 'react'
import { forgotPassword } from "../auth/auth";

export default class ForgotPassword extends Component {
    state = {
        email: "",
        message: "",
        error: ""
    };
 
    forgotPassword = e => {
        e.preventDefault();
        this.setState({ message: "", error: "" });
        forgotPassword(this.state.email).then(data => {
            if (data.error) {
                console.log(data.error);
                this.setState({ error: data.error });
            } else {
                console.log(data.message);
                this.setState({ message: data.message });
            }
        });
    };
 
    render() {
        return (
            <div className="container mt-4">
                <div className="col-md-6 mx-auto mt-4 card">
                <h1 className="text-center mb-4 mt-4 card-header">Reset Password ツ</h1>
 
                {this.state.message && (
                    <h4 className="alert alert-info text-center">{this.state.message}</h4>
                )}
                {this.state.error && (
                    <h4 className="alert alert-danger text-center">{this.state.error}</h4>
                )}
 
                    <form className="card-body">
                    <div className="form-groupe">
                    <label>Type your Email</label>
                        <input
                            type="email"
                            className="form-control"
                            placeholder="Your email address"
                            value={this.state.email}
                            name="email"
                            onChange={e =>
                                this.setState({
                                    email: e.target.value,
                                    message: "",
                                    error: ""
                                })
                            }
                            autoFocus
                        />
                    </div>
                    <hr/>
                    <button
                        onClick={this.forgotPassword}
                        className="btn btn-secondary"
                    >
                        Send Password Rest Link
                    </button>
                </form>
            </div>
            </div>
        );
    }
}
