import React, { Component } from 'react'
import { findPeople, follow } from './apiUser'
import defaultAvatar from '../img/avatar.jpg'
import {isAuthenticated} from '../auth/auth'
import { toast } from 'react-toastify';


export default class FindPeople extends Component {
    constructor(){
        super()
        this.state = {
            users: [],
            error: '',
            // open: false
        }
    }

    componentDidMount() {
        const userId = isAuthenticated().user._id
        const token = isAuthenticated().token
        
        findPeople(userId, token)
        .then(data => {
            if (data.error) {
                console.log(data.error)
            } else {
                this.setState({users: data})
            }
        })
    }
    

    clickFollow = (user, i) => {
        const userId = isAuthenticated().user._id;
        const token = isAuthenticated().token;

        follow(userId, token, user._id).then(data => {
            if (data.error) {
                this.setState({ error: data.error });
            } else {
                let toFollow = this.state.users;
                toFollow.splice(i, 1);
                this.setState({
                    users: toFollow,
                    // open: true,
                    // followMessage: `Following ${user.name}`
                });
            }
        });
    };

    renderUsers = users => {
        toast.configure()
        
        const notify = () => {
            toast.dark('Followed Successfully', {
                position: "top-center",
                autoClose: 2000,
                hideProgressBar: false,
                closeOnClick: true,
                pauseOnHover: false,
                draggable: true,
                progress: undefined,
                });
        }
        return <div>
        <div className="row mt-4">
        {users.map((user, i) => (
            <div className="card col-md-3" key={i}>
            <img src={`${process.env.REACT_APP_API_URL}/user/photo/${user._id}?${new Date().getTime()}`} onError={i => (i.target.src=`${defaultAvatar}`)} className="card-img-top mt-3" style={{height:'250px'}} alt="profile"/>
            <div className="card-body">
                <h5 className="card-title text-center">{user.name}</h5>
                <button onClick={ ()=> {notify(this.clickFollow(user, i))}}  className="btn btn-light btn-block"> Follow </button>
                {/* <button onClick={notify} className="btn btn-light btn-block"> Test </button> */}
            </div>
            </div>
        ))}
    </div>
    </div>
    }

    render() {
        const {users} = this.state;
        
        return (
            <div className="container">
                
                <h2 className="mt-4 card-header text-center">Find People</h2>

                {
                    // open && (<div className=" alert alert-success mt-3 ">{followMessage} </div>)
                }
                {this.renderUsers(users)}
                    {/* <button onClick={notify}>test</button> */}
                </div>
                
        )
    }
}
