import React, { Component } from 'react';
import { Redirect } from 'react-router-dom';
import {isAuthenticated, signout} from '../auth/auth';
import { remove } from './apiUser';


export default class DeleteUser extends Component {
    
    state = {
        redirect: false,
        redirectToAdmin: false
    };

    deleteAccount = () => {
        const token = isAuthenticated().token;
        const userId = this.props.userId;
        remove(userId, token).then(data => {
            if (data.error) {
                console.log(data.error);
            } else if (isAuthenticated().user.role === "admin") {
                this.setState({
                    redirectToAdmin: true
                });
            } else {
                // signout user
                signout(() => console.log("User is deleted"));
                // redirect
                this.setState({ redirect: true });
            }
        });
    };

    deleteConfirmed = () => {
        let answer = window.confirm(
            "Are you sure you want to delete your account?"
        );
        if (answer) {
            this.deleteAccount();
        }
    };

    render() {
        const {redirect, redirectToAdmin} = this.state

        if (redirect) {
            return <Redirect to='/' />
        }

        if (redirectToAdmin) {
            return <Redirect to='/admin' />
        }
        return (
           
            <span style={{cursor:'pointer'}} onClick={this.deleteConfirmed} className="badge badge-danger"><h5>Delete Profile</h5></span>
        )
    }
}
