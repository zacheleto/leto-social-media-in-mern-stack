import React, { Component } from 'react'
import { Redirect, Link } from 'react-router-dom';
import { signIn, authenticateUser } from '../auth/auth';
import loadingGif from '../img/loading.gif'
import SocialLogin from "./SocialLogin";

export default class Signin extends Component {
    constructor() {
        super()
        this.state = {
            email: '',
            password: '',
            error: '',
            redirectUser: false,
            loading: false
        }
        
    }

    handleChange = name => event => {
        this.setState({error: ''});
        
        this.setState({[name]: event.target.value});
    };

    signInUser = e => {

        e.preventDefault()
        this.setState({loading:true})
        const {email, password} = this.state
        const user = {
                        email,
                        password
                     };

       signIn(user)
        .then(data => {
            if (data.error) {
                this.setState({error: data.error, loading:false})
            } else {
                //authenticate and redirect the user
                authenticateUser(data, () => {
                    this.setState({redirectUser: true})
                })
                
                
            }
        })
                     
    };



    signInForm = (email, password) => (
        <form className="card-body">
         <div className="form-groupe">
             <label>Email</label>
             <input onChange={this.handleChange('email')} type="text" className="form-control" value={email}/>
         </div>
         <br/>
         <label>Password</label>
         <div className="form-groupe">
             <input onChange={this.handleChange('password')} type="password" className="form-control" value={password}/>
         </div>
         <hr/>
         <p className="text-center">
            <Link to="/forgot-password" className="text-danger">
                {" "}
                Forgot Password
            </Link>
            <span className="ml-4">New user! <Link to={`/signup`}> SIGNUP </Link></span>
        </p>
         <button onClick={this.signInUser} className="btn btn-secondary">Login</button>
       </form>
    );


    render() {
        const {email, password, error, redirectUser, loading} = this.state;
        if (redirectUser) {
            return <Redirect to="/"/>
        }
        return (
            <div className="container mt-4">
                <div className="col-md-6 mx-auto mt-4 card">
                <h1 className="text-center mb-4 mt-4 card-header">Login ツ</h1>
             
                <SocialLogin />
           
                    <div id="alert" className="alert alert-info text-center" style={{display: error ? '' : 'none'}}>{error}</div>
                    {loading ? (
                    
                    <div className="text-center"><img src={loadingGif} alt="loading gif" height="260px" width="200px"></img></div>
                    
                    ) : this.signInForm(email, password)}
                </div>
               
            </div>
        );
    }
}
