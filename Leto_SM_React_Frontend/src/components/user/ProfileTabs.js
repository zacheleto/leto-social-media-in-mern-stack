import React, { Component } from 'react'
import { Link } from 'react-router-dom';
import defaultAvatar from '../img/avatar.jpg'
import defaultPostAvatar from '../img/postavatar.jpg'

export default class ProfileTabs extends Component {
    
    render() {
        const {following, followers, posts} = this.props
        return (
          <div className="row mt-3">
            <div className="col-md-4">
                <h3 className="card-header">Followers</h3>
                <div className="card mb-3 scrollable">
                {followers.map((user, i) => {
                  return (
                    <ul key={i} className="list-group list-group-flush">
                      <Link to={`/user/${user._id}`}>
                        <li className="list-group-item">
                          <img
                            style={{ height: "50px", width: "50px" }}
                            className="mr-3"
                            onError={(i) => (i.target.src = `${defaultAvatar}`)}
                            src={`${process.env.REACT_APP_API_URL}/user/photo/${
                              user._id
                            }?${new Date().getTime()}`}
                            alt={user.name}
                          />
                          {user.name}
                        </li>
                      </Link>
                    </ul>
                  );
                })}
              </div>
            </div>
            <div className="col-md-4">
                <h3 className="card-header">Following</h3>
                <div className="card mb-3 scrollable">
                {following.map((user, i) => {
                  return (
                    <ul key={i} className="list-group list-group-flush">
                      <Link to={`/user/${user._id}`}>
                        <li className="list-group-item">
                          <img
                            style={{ height: "50px", width: "50px" }}
                            className="mr-3"
                            onError={(i) => (i.target.src = `${defaultAvatar}`)}
                            src={`${process.env.REACT_APP_API_URL}/user/photo/${
                              user._id
                            }?${new Date().getTime()}`}
                            alt={user.name}
                          />
                          {user.name}
                        </li>
                      </Link>
                    </ul>
                  );
                })}
              </div>
            </div>
            <div className="col-md-4">
              
                <h3 className="card-header">Recent Posts</h3>
                <div className="card mb-3 scrollable">
                {posts.map((post, i) => {
                  return (
                    <ul key={i} className="list-group list-group-flush">
                      <Link to={`/post/${post._id}`}>
                        <li className="list-group-item">
                          <img
                            style={{ height: "50px", width: "50px" }}
                            className="mr-3"
                            onError={(i) => (i.target.src = `${defaultPostAvatar}`)}
                            src={`${process.env.REACT_APP_API_URL}/post/photo/${post._id}?${new Date().getTime()}`}
                            alt={post.title}
                          />
                          {post.title}
                        </li>
                      </Link>
                    </ul>
                  );
                })}
              </div>
            </div>
          </div>
        );
    }
}
