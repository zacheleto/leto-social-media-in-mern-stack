import React, { Component } from 'react'
import {follow, unfollow} from './apiUser'

export default class FollowProfileButton extends Component {
    followClick = () => {
        this.props.onButtonClick(follow);
    };

    unfollowClick = () => {
        this.props.onButtonClick(unfollow);
    };

    render() {
        
        return (
            <div className="mt-3">
           {
            !this.props.following ?  (<button  onClick={this.followClick} className="btn btn-light btn-block">Follow</button>) 
             : (<button onClick={this.unfollowClick}  className="btn btn-light btn-block">UnFollow</button>)
           }
            </div>    
          
        )
    }
}
