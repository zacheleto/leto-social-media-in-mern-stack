import React, { Component } from 'react'
import { resetPassword } from "../auth/auth";

export default class ResetPassword extends Component {
    constructor(props) {
        super(props);
        this.state = {
            newPassword: "",
            message: "",
            error: ""
        };
    }
 
    resetPassword = e => {
        e.preventDefault();
        this.setState({ message: "", error: "" });
 
        resetPassword({
            newPassword: this.state.newPassword,
            resetPasswordLink: this.props.match.params.resetPasswordToken
        }).then(data => {
            if (data.error) {
                console.log(data.error);
                this.setState({ error: data.error });
            } else {
                console.log(data.message);
                this.setState({ message: data.message, newPassword: "" });
            }
        });
    };
 
    render() {
        return (
            <div className="container mt-4">
                <div className="col-md-6 mx-auto mt-4 card">
                <h1 className="text-center mb-4 mt-4 card-header">Reset Password ツ</h1>
 
                {this.state.message && (
                    <h4 className="alert alert-success text-center">{this.state.message}</h4>
                )}
                {this.state.error && (
                    <h4 className="alert alert-danger text-center">{this.state.error}</h4>
                )}
 
                <form>
                    <div className="form-groupe">
                        <input
                            type="password"
                            className="form-control"
                            placeholder="Your new password"
                            value={this.state.newPassword}
                            name="newPassword"
                            onChange={e =>
                                this.setState({
                                    newPassword: e.target.value,
                                    message: "",
                                    error: ""
                                })
                            }
                            autoFocus
                        />
                    </div>
                    <hr/>
                    <button
                        onClick={this.resetPassword}
                        className="btn btn-secondary mb-4"
                    >
                        Reset Password
                    </button>
                </form>
            </div>
            </div>
        );
    }
}
