import React, { Component } from "react";
import { withRouter} from 'react-router-dom';

export class Footer extends Component {
  render() {
    return (
      <div>
        <footer className="mt-3 mb-4 p-3 text-center">
          Zache Abdelatif, 2020 &copy;
        </footer>
      </div>
    );
  }
}

export default withRouter(Footer);
