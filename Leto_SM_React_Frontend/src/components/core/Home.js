import React, { Component } from 'react'
import Posts from '../post/Posts'

export default class Home extends Component {
    render() {
        return (
                <div className="container mt-4">
                    <div className="jumbotron">
                        <h2>Home</h2>
                        <p className="lead">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries</p>
                            
                    </div>
                    <Posts />
                </div>
            )
    }
}


