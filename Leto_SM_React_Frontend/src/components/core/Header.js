import React, { Component } from 'react';
import {Link, withRouter} from 'react-router-dom';
import { signout, isAuthenticated } from '../auth/auth';

export class Header extends Component {


    render() {
        const path = this.props.history.location.pathname;
        return (
           
            
            <div>
               <nav className="navbar navbar-expand-lg navbar-dark bg-primary">
               <Link className="navbar-brand" to="/">Leto Social Media ツ</Link>
                <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarColor01" aria-controls="navbarColor01" aria-expanded="false" aria-label="Toggle navigation">
                <span className="navbar-toggler-icon"></span>
                </button>
                
                <div className="collapse navbar-collapse" id="navbarColor01">
                    <ul className="navbar-nav mr-auto">
                        
                        <li className={path === '/' ? "nav-item active" : ''}>
                        <Link className="nav-link" to="/">Home <span className="sr-only">(current)</span></Link>
                        </li>
                        <li className={path === '/users' ? "nav-item active" : ''}>
                        <Link className="nav-link" to="/users">Users <span className="sr-only">(current)</span></Link>
                        </li>
                        {!isAuthenticated() && (
                           <div>
                            <ul className="navbar-nav mr-auto">                
                                <li className={path === '/signup' ? "nav-item active" : ''}>
                                <Link className="nav-link" to="/signup">signup</Link>
                                </li>
                            
                                <li className={path === '/signin' ? "nav-item active" : ''}>
                                <Link className="nav-link" to="/signin">Login</Link>
                                </li>
                            </ul>  
                            </div>
                            
                        )}
                        {isAuthenticated() && (
                            <>
                                
                                <li className={path === '/findpeople' ? "nav-item active" : ''}>
                                <Link className="nav-link" to="/findpeople">Find People</Link>
                                </li>
                                <li className={path === `/user/${isAuthenticated().user._id}` ? "nav-item active nav-item dropdown" : 'nav-item dropdown'}>
                                    <div className="nav-link dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><span className="text-light">Hi </span><span className="text-info">{isAuthenticated().user.name}!</span></div>
                                    <div className="dropdown-menu">
                                    <Link className="dropdown-item" to={`/user/${isAuthenticated().user._id}`}>Profile</Link>
                                    <Link className="dropdown-item" to="#">Another action</Link>
                                    <Link className="dropdown-item" to="#">Something else here</Link>
                                    <div className="dropdown-divider"></div>
                                    <Link className="dropdown-item" to="#">Separated link</Link>
                                    </div>
                                </li>
                                <li style={{cursor:'pointer'}}>
                                    <div onClick={() => signout(() =>this.props.history.push('/'))} className="nav-link">Logout</div>
                                </li>
                            </>
                        )}
                        {isAuthenticated() && isAuthenticated().user.role === "admin" && (
                        <li className={path === '/admin' ? "nav-item active" : ''}>
                            <Link className="nav-link" to="/admin">Admin</Link>
                        </li>
                        )}
                    </ul>
                    </div>
                </nav>  
            </div>

            
        )
       
    }
    
}

export default withRouter(Header);
