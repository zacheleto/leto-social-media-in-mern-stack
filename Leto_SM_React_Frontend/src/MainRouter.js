import React from 'react';
import {Route, Switch} from 'react-router-dom';
import Home from './components/core/Home';
import Signup from './components/user/Signup';
import Signin from './components/user/Signin';
import Header from './components/core/Header';
import Profile from './components/user/Profile';
import Users from './components/user/Users';
import EditProfile from './components/user/EditProfile';
import PrivateRoute from './components/auth/PrivateRoute';
import FindPeople from './components/user/FindPeople';
import NewPost from './components/post/NewPost';
import SinglePost from './components/post/SinglePost';
import Footer from './components/core/Footer';
import EditPost from './components/post/EditPost';
import ForgotPassword from './components/user/ForgotPassword';
import ResetPassword from './components/user/ResetPassword';
import Admin from './components/admin/Admin';

const MainRouter = () => (
    <div>
        <Header/>
        <Switch>
            <Route exact path="/" component={Home} />
            <Route exact path="/users" component={Users} />
            <Route exact path="/signup" component={Signup} />
            <Route exact path="/signin" component={Signin} />
            <PrivateRoute exact path="/findpeople" component={FindPeople} />
            <PrivateRoute exact path="/user/:userId" component={Profile} />
            <PrivateRoute exact path="/user/edit/:userId" component={EditProfile} />
            <PrivateRoute exact path="/post/create" component={NewPost} />
            <Route exact path="/post/:postId" component={SinglePost} />
            <PrivateRoute exact path="/post/edit/:postId" component={EditPost} />
            <Route exact path="/forgot-password" component={ForgotPassword} />
            <Route exact path="/reset-password/:resetPasswordToken" component={ResetPassword} />
            <PrivateRoute exact path="/admin" component={Admin} />


            
        </Switch>
        <Footer />

    </div>
)

export default MainRouter;